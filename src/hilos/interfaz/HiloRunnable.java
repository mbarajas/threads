/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos.interfaz;

import hilos.CorrerHilos;
import hilos.interfaz.HiloJFrame;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maria
 */
public class HiloRunnable implements Runnable {

    private String nomHilo;
    private int tiempoHilo;

    public HiloRunnable(String nomHilo, int tiempoHilo) {
        this.nomHilo = nomHilo;
        this.tiempoHilo = tiempoHilo;
    }

    @Override
    public void run() {

        String[] abecedario = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
            "K", "L", "M", "N", "Ñ", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

        String nombreFichero = null;
        
        while (true) {
            int numRandon = (int) Math.floor(Math.random()*27);
            nombreFichero = abecedario[numRandon];
            try {
                FileWriter fw = new FileWriter(this.nomHilo + "\\" + nombreFichero + ".txt");
                System.out.println(this.nomHilo + "\\" + nombreFichero + ".txt");
            } catch (IOException ex) {
                Logger.getLogger(HiloRunnable.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                System.out.println("Hilo a dormir [" + this.tiempoHilo + "]");
                Thread.currentThread().sleep(this.tiempoHilo);
            } catch (InterruptedException ex) {
                Logger.getLogger(HiloRunnable.class.getName()).log(Level.SEVERE, null, ex);
                break;
            }
        }

    }

}
