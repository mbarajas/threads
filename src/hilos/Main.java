/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import hilos.interfaz.HiloJFrame;
import hilos.runn.HiloRunn;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 *
 * @author Maria
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        HiloJFrame hiloJFrame = new HiloJFrame();

        String[] abecedario = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
            "K", "L", "M", "N", "Ñ", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
       
        String nombreFichero = null;
        Random random = new Random(System.currentTimeMillis());
        
        for (int i = 0; i < abecedario.length; i++) {
            int numRandon = (int) Math.round(Math.random() * 28);
            //System.out.println(abecedario[numRandon]);
            nombreFichero = abecedario[numRandon];
            FileWriter fw = new FileWriter("C:\\Hilo\\"+nombreFichero+".txt");
            HiloRunn runnable = new HiloRunn("nombreFichero ", 28);
            //runnable.setTiempoHilo(random.nextInt(5001));
            CorrerHilos ejecutaHilos = new CorrerHilos((Runnable) runnable);
            ejecutaHilos.start();
        }

    }

    public Main() {

    }

}
